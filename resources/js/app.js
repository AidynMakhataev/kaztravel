require('./bootstrap');

require('./components/phone-mask');
require('./components/reviews-slider');
require('./components/reports-inner');
require('./components/tour-inner');
require('./components/cart');
require('./components/header');
require('./components/home-hero');
require('./components/mob-menu');

window.Vue = require('vue');

import CartButton from "./components/vue/CartButton.vue";
import CartItemList from "./components/vue/CartItemList.vue";
import UpsellItem from "./components/vue/UpsellItem.vue";
import CartUpsellItemList from "./components/vue/CartUpsellItemList";

Vue.component('cart-button', CartButton)
Vue.component('cart-item-list', CartItemList)
Vue.component('upsell-item', UpsellItem)
Vue.component('cart-upsell-item-list', CartUpsellItemList)


if (document.getElementById('app')) {
    const app = new Vue({
        el: '#app'
    });
}
