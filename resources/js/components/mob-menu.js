import gsap from 'gsap'

window.addEventListener('load', function () {
    const tl = gsap.timeline()
    const burger = document.querySelector('.mob-header-burger')
    const menu = document.querySelector('.mobMenu')
    const close = menu.querySelector('.mobMenu-close')

    tl 
    .fromTo(menu, 
        {css:{'display':'none'}},
        {css:{'display':'block'}, duration: 0.1}
    )
    .fromTo(menu,
        {y: 20, opacity: 0},
        {y: 0, opacity: 1, duration: 0.4},
        0.1
    )

    tl.reverse()

    burger.addEventListener('click', function () {
        tl.reversed(!tl.reversed())
    })
    close.addEventListener('click', function () {
        tl.reversed(!tl.reversed())
    })
})