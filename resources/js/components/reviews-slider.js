import Swiper from 'swiper'

window.addEventListener('load', () => {
    new Swiper('.reviews-slider', {
        slidesPerView: 1,
        pagination: {
            el: '.reviewsSlider-pagination',
            type: 'bullets',
            clickable: true
        }
    })
})