import gsap from 'gsap'
window.addEventListener('load', () => {
    if (document.querySelector('.cart-remove')) {
        const modal = document.querySelector(".cartRemove")
        const overlay = modal.querySelector('.overlay')
        overlay.addEventListener('click', () => {
            modal.classList.remove('-open')
        })
    }
    // if (window.innerWidth < 769) {
    //     if (document.querySelector('.cartAddModal')) {
    //         const addModal = document.querySelector('.cartAddModal')
    //         const addOverlay = addModal.querySelector('.cartAddModal-overlay')
    //         const addInner = addModal.querySelector('.cartAddModal-inner')
    //         const addBtn = document.querySelectorAll('.cart-mobAdd')
    //
    //         const addTl = gsap.timeline()
    //
    //         addTl
    //         .fromTo(addModal,
    //             {css: {'display': 'none'}},
    //             {css: {'display':'flex'}, duration: 0.1}
    //         )
    //         .fromTo(addOverlay,
    //             {opacity: 0},
    //             {opacity: 1, duration: 0.2},
    //             0.2
    //         )
    //         .fromTo(addInner,
    //             {yPercent: 110},
    //             {yPercent: 0, duration: 0.4, ease: 'power2.out'},
    //             0.2
    //         )
    //
    //         addTl.reverse()
    //
    //         addOverlay.addEventListener('click', function () {
    //             addTl.reversed(!addTl.reversed())
    //         })
    //
    //         addBtn.forEach(btn => {
    //             btn.addEventListener('click', function () {
    //                 addTl.reversed(!addTl.reversed())
    //             })
    //         })
    //     }
    // }
})
