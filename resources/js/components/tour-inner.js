import Swiper from "swiper";
import "lightgallery.js";

window.addEventListener("load", function() {
    lightGallery(document.querySelector("#tour-inner-gallery"), {
        selector: ".gallery-block"
    });

    new Swiper(".tour-inner-similar", {
        slidesPerView: 1,
        pagination: {
            el: ".tour-inner-similar .swiper-pagination",
            type: "bullets",
            clickable: true
        }
    });

    new Swiper(".mobile-reviews-slider", {
        slidesPerView: 1,
        spaceBetween: 20,
        pagination: {
            el: ".mobile-reviews .swiper-pagination",
            type: "bullets",
            clickable: true
        }
    });

    document
        .querySelectorAll('.tour-inner-header a[href^="#"]')
        .forEach(trigger => {
            trigger.onclick = function(e) {
                e.preventDefault();
                let hash = this.getAttribute("href");
                let target = document.querySelector(hash);
                let headerOffset = 70;
                let elementPosition = target.offsetTop;
                let offsetPosition = elementPosition - headerOffset;

                window.scrollTo({
                    top: offsetPosition,
                    behavior: "smooth"
                });
            };
        });
});
