import gsap from 'gsap'
window.addEventListener('load', () => {
    if (document.querySelector(".js-header-sticky")) {
        const stickyHeader = document.querySelector(".js-header-sticky")
        window.addEventListener('scroll', () => {
            if (pageYOffset > 60) {
                stickyHeader.classList.add('-fixed')
            } else if (pageYOffset < 60) {
                stickyHeader.classList.remove('-fixed')
            }
        })
    }

    const header = document.querySelector(".header")
    const burger = document.querySelector(".header-burger")
    const overlay = header.querySelector('.overlay')
    const tl = gsap.timeline()

    tl
        .fromTo(header,
            { height: '123px' },
            { height: '110%', duration: 1.4, ease: 'expo.inOut' }
        )
    tl.reverse()
    burger.addEventListener('click', function () {
        tl.reversed(!tl.reversed())
    })
})
