import Swiper from 'swiper'
import 'lightgallery.js'
window.addEventListener('load', () => {

    lightGallery(document.querySelector('#reports-inner-gallery'), {
        selector: '.reports-block'
    })
    
    new Swiper('.reports-inner-similar', {
        slidesPerView: 4,
        pagination: {
            el: '.reports-inner-similar .swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        breakpoints: {
            320: {
                slidesPerView: 1
            },
            769: {
                slidesPerView: 2
            },
            1024: {
                slidesPerView: 4
            }
        }
    })
})