import Plyr from 'plyr'
window.addEventListener('load', function () {
    const player = new Plyr('#home-video', {
        controls: [
            'play-large',
            'play',
            'progress',
            'current-time',
            'mute',
            'volume',
            'fullscreen',
        ]
    })
    const btn = document.querySelector(".hero-playBtn")
    if (btn) {
        btn.addEventListener('click', function () {
            player.fullscreen.toggle()
            player.play()
        })
    }

    player.on('exitfullscreen', function () {
        player.stop()
    })
})