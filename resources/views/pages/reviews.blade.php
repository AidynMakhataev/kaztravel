@extends('layout')

@section('content')
    @include('components.reviews-slider')

    @if(isset($blocks['section_advantages']))
        @include('components.advantages', ['block' => $blocks['section_advantages']])
    @endif
@endsection
