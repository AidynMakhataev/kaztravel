@extends('layout')

@section('content')
    @if(isset($blocks['section_hero']))
        @include('components.home.hero', ['block' => $blocks['section_hero']])
    @endif

    @if(isset($tours) && count($tours) > 0)
        @include('components.home.tours')
    @endif

    @if(isset($blocks['section_advantages']))
        @include('components.advantages', ['block' => $blocks['section_advantages']])
    @endif

    @if(isset($reviews) && count($reviews) > 0)
        @include('components.reviews-slider', ['reviews' => $reviews])
    @endif
@endsection
