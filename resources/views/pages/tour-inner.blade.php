@extends('layout')

@section('content')
    @php
     /** @var \App\Models\Tour $tour */
    @endphp

    @include('components.tour-inner.header')
    @include('components.tour-inner.hero')

    @if($tour->getDecodedImagesBlock() !== null)
        @include('components.tour-inner.gallery')
    @endif

    @if($tour->getDecodedRouteMapBlock() !== null)
        @include('components.tour-inner.program', ['block' => $tour->getDecodedRouteMapBlock()])
    @endif

    @include('components.tour-inner.callback')

    @if($tour->getDecodedPriceBlock() !== null)
        @include('components.tour-inner.price')
    @endif

    @if($tour->getDecodedUpsellBlock() !== null)
        @include('components.tour-inner.extras')
    @endif

    @if($tour->getDecodedDetailBlock() !== null)
        @include('components.tour-inner.details')
    @endif

    @if(count($tour->reviews) > 0)
        <div id="tour-reviews">
            @include('components.reviews-slider', ['reviews' => $tour->reviews])
        </div>
        @endif

    {{-- MOBILE REVIEWS --}}
    @include('components.tour-inner.mobile-reviews')

{{--    @include('components.tour-inner.similar')--}}

    @if(isset($blocks['section_advantages']))
        @include('components.advantages', ['block' => $blocks['section_advantages']])
    @endif
    <a href="" class="whatsapp-btn">
        <img src="/img/whatsapp.svg" alt="icon">
    </a>
@endsection
