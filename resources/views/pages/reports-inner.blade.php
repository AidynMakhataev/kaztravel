@extends('layout')

@section('content')
    @include('components.reports-inner.hero')
    @include('components.reports-inner.grid')
{{--    @include('components.reports-inner.reviews')--}}

    @if(count($similar) > 0)
        @include('components.reports-inner.similar')
    @endif
    @isset($blocks['section_advantages'])
        @include('components.advantages', ['block' => $blocks['section_advantages']])
    @endisset
@endsection
