@extends('layout')

@section('content')
    @php
        $blocks = \App\Models\Block::getBlocks(['section_advantages', 'section_success']);
    @endphp

    <div class="success">
        <img src="/images/circle-check.svg" alt="icon" class="success-icon">
        <h1 class="success-title">{{ $blocks['section_success']['title'] ?? 'Путевка успешно приобретена!' }}</h1>
        <p class="success-sub">{{ $blocks['section_success']['subtitle'] ?? 'Мы отправим вам билет на почту' }}</p>
        <a href="/" class="success-link">Вернуться на главную</a>
        @isset($blocks['section_advantages'])
            @include('components.advantages', ['block' => $blocks['section_advantages']])
        @endisset
    </div>
@endsection
