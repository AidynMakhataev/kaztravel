@extends('layout')

@section('content')
    <div class="about-page">
        @isset($blocks['section_about_hero'])
            @include('components.about.hero', ['block' => $blocks['section_about_hero']])
        @endisset


        @isset($blocks['section_about_team'])
            @include('components.about.staff', ['block' => $blocks['section_about_team']])
        @endisset

        @isset($blocks['section_advantages'])
            @include('components.advantages', ['block' => $blocks['section_advantages']])
        @endisset

{{--        @include('components.reviews-slider')--}}
    </div>
@endsection
