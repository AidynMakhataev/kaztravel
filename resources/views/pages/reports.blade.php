@extends('layout')

@section('content')
    @include('components.reports.content')

    @isset($blocks['section_advantages'])
        @include('components.advantages', ['block' => $blocks['section_advantages']])
    @endisset
@endsection
