<section class="hero -tourInner" style="background-image: url('../{{ $block['image'] }}');">
    <div class="overlay"></div>
    <div class="hero-inner">
        <h1 class="hero-title">{{ $block['title'] }}</h1>
        <p class="hero-subtitle">{{ $block['subtitle'] }}</p>
    </div>
</section>
