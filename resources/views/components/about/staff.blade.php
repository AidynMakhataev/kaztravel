<div class="about-staff">
    <h2 class="title">{{ $block['title'] }}</h2>
    <div class="about-staff-inner">
        <div class="row">
            @foreach($block['items'] as $item)
                <div class="about-staff-item">
                    <img src="/{{ $item['avatar'] }}" alt="{{ $item['name'] }}" class="about-staff-img">
                    <div class="about-staff-info">
                        <p class="about-staff-name">{{ $item['name'] }}</p>
                        <p class="about-staff-position">{{ $item['position'] }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
