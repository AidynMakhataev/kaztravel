<section class="reports">
    <div class="row">
        @foreach($reports as $report)
            @php
                /** @var \App\Models\Report $report */
            @endphp
            <div class="col">
                <a href="{{ route('reports.show', ['slug' => $report->slug]) }}" class="reports-link">
                    <img src="/{{ $report->bg_image }}" alt="{{ $report->title }}" class="reports-img">
                    <div class="overlay"></div>
                    <p class="reports-name">{{ $report->title }}</p>
                </a>
            </div>
        @endforeach
    </div>
</section>
