<header class="header {{ Route::currentRouteName() != 'tour-inner' ? 'js-header-sticky' : '' }}">
    <div class="header-top">
        <div class="header-container">
            <div class="row">
                <div class="col">
                    <p class="header-heading">{{ __('site.header_heading_text') }}</p>
                </div>
                <div class="col -jcfe">
                    <p class="header-text">{{ __('site.feedback') }}</p>
                    <div class="header-phoneWrp">
                        <img src="/svg/dropdown-arrow.svg" alt="Phone arrow">
                        <div class="header-currentPhone">
                            <a href="tel:{{ \Backpack\Settings\app\Models\Setting::get('main_phone') }}" class="header-phone">{{ \Backpack\Settings\app\Models\Setting::get('main_phone') }}</a>
                            <p class="header-phoneText">{{ __('site.phone_subtext') }}</p>
                        </div>
                    </div>
                    <div class="header-langWrp">
                        <div class="header-currentLang">
                            <a href="" class="header-lang">RU</a>
                            <img src="/svg/dropdown-arrow.svg" alt="Phone arrow">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bot">
        <div class="header-container">
            <button class="header-burger">
                <span></span>
            </button>
            <div class="row -jcsb -aic">
                <a href="/" class="header-logo">
                    <b>Kazakhstan</b> Travel
                </a>
                <nav class="header-nav">
                    <a href="/#tours" class="header-link">
                        <span class="header-text">{{ __('site.tours') }}</span>
                    </a>
                    <a href="{{ route('reports') }}" class="header-link">
                        <span class="header-text">{{ __('site.photos') }}</span>
                    </a>
                    <a href="{{ route('reviews') }}" class="header-link">
                        <span class="header-text">{{ __('site.reviews') }}</span>
                    </a>
                    <a href="{{ route('about') }}" class="header-link">
                        <span class="header-text">{{ __('site.about_us') }}</span>
                    </a>
                    <a href="{{ route('contacts') }}" class="header-link">
                        <span class="header-text">{{ __('site.contacts') }}</span>
                    </a>
                </nav>
                <a href="/cart" class="header-cartLink">
                    <span class="header-counter">{{ $cartItemsCount }}</span>
                    <img src="/svg/shopping-bag.svg" alt="Cart icon">
                </a>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
    @include('components.menu')
</header>
