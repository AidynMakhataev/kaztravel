<section class="mob-advs {{Route::currentRouteName() === 'contacts' ? '-contacts' : ''}}">
    <div class="container">
        <h3 class="title">Kazakhstan <span>Travel</span></h3>
        <p class="text">Преимущества путешествий вместе с нами</p>
        <div class="row">
            <div class="mob-advs-icon">
                <img src="/svg/backpack-icon.svg" alt="Kazakhstan Travel icon">
            </div>
            <div class="mob-advs-right">
                <h4 class="mob-advs-subtitle">Насыщенная программа</h4>
                <p class="mob-advs-text">
                    Максимум впечатлений
                    в каждой поездке — от парада пингвинов в ЮАР до 
                    сэндбординга в Сахаре.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="mob-advs-icon">
                <img src="/svg/map-icon.svg" alt="Kazakhstan Travel icon">
            </div>
            <div class="mob-advs-right">
                <h4 class="mob-advs-subtitle">Погружение в страну</h4>
                <p class="mob-advs-text">
                    Мы протестировали каждый маршрут и собрали вишенки
                    с тортов для вас
                </p>
            </div>
        </div>
        <div class="row">
            <div class="mob-advs-icon">
                <img src="/svg/boy-icon.svg" alt="Kazakhstan Travel icon">
            </div>
            <div class="mob-advs-right">
                <h4 class="mob-advs-subtitle">Забота о каждом клиенте</h4>
                <p class="mob-advs-text">
                    Знакомство перед поездкой, 
                    авторский путеводитель по стране, консультации 
                    и поддержка.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="mob-advs-icon">
                <img src="/svg/contract-icon.svg" alt="Kazakhstan Travel icon">
            </div>
            <div class="mob-advs-right">
                <h4 class="mob-advs-subtitle">Договор без уловок</h4>
                <p class="mob-advs-text">
                    Фиксированная стоимость путешествия. Выполнение программы на 99%.      
                </p>
            </div>            
        </div>
    </div>
</section>