<div class="menu">
    <div class="menu-headingWrp">
        <img src="/images/menu-banner.jpg" alt="Menu banner">
        <div class="menu-headingOverlay"></div>
        <div class="header-container">
            <p class="menu-heading">Путешествуйте <b>вместе с нами!</b></p>
        </div>
    </div>
    <div class="menu-bot">
        <div class="header-container">
            <div class="row">
                @php
                    /** @var \Illuminate\Database\Eloquent\Collection $tours */
                @endphp


                @foreach ($tours->chunk(5) as $chunk)
                    <div class="col">
                        <ul class="menu-list">
                            @foreach ($chunk as $tour)
                                <li class="menu-listItem"><a href="{{ route('tour.show', ['slug' => $tour->slug]) }}">{{ $tour->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
