<section class="hero {{Route::currentRouteName() === 'home' ? '-home' : ''}}" style="background-image: url('../{{ $block['image'] }}');">
    <div class="overlay"></div>
    <div class="hero-inner">
        <h1 class="hero-title">{{ $block['title'] }}</h1>
        <h2 class="hero-subtitle">{{ $block['subtitle'] }}</h2>
    </div>
</section>
