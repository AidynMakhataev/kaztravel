<section class="homeTours" id="tours">
    <h2 class="homeTours-title">Туры и экспедиции</h2>
    <div class="row homeTours-row">
        @foreach($tours as $tour)
            <div class="col">
                <div class="homeTours-block">
                    <span class="homeTours-label -violet">{{ $tour->tag_text }}</span>
                    <a href="{{ route('tour.show', ['slug' => $tour->slug ]) }}" class="overlay"></a>
                    <img src="/{{ $tour->bg_image }}" alt="{{ $tour->title }}" class="homeTours-img -small">
                    <div class="homeTours-nameWrp">
                        <a href="{{ route('tour.show', ['slug' => $tour->slug]) }}" class="homeTours-name">{{ $tour->title }}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>
