<header class="mob-header">
    <div class="container">
        <div class="row -aic">
            <button class="mob-header-burger">
                <span></span>
            </button>
            <a href="/" class="mob-header-logo">
                KZT<span>ravel</span>
            </a>
            <a href="tel:{{ \Backpack\Settings\app\Models\Setting::get('main_phone') }}" class="mob-header-phone">
                <span>{{ \Backpack\Settings\app\Models\Setting::get('main_phone') }}</span>
                <img src="/svg/phone-icon.svg" alt="Kazakh Travel icon">
            </a>
            <a href="/cart" class="mob-header-cart">
                <span class="mob-header-counter">{{ $cartItemsCount }}</span>
                <img src="/svg/shopping-bag.svg" alt="Cart icon">
            </a>
        </div>
    </div>
</header>
