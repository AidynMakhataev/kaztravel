<footer class="footer {{Route::currentRouteName() == 'contacts' ? '-full-height' : ''}} {{Route::currentRouteName() == 'tour-inner' ? '-tourInner' : ''}}">

    @if(isset($blocks['section_callback']))
        <div class="footer-top" style="background-image: url('../{{ $blocks['section_callback']['image'] }}');">
        <div class="overlay"></div>
        <div class="footer-inner">
            <h3 class="footer-title">{{ $blocks['section_callback']['title'] }}</h3>
            <h4 class="footer-subtitle">{{ $blocks['section_callback']['subtitle'] }}</h4>
            <form action="{{ route('callback') }}" method="POST" class="footer-form">
                {{ csrf_field() }}
                <input type="text" name="name" placeholder="Ваше имя" required="required" class="footer-input">
                <input type="tel" name="phone" placeholder="Телефон" required="required" class="footer-input phone-mask">
                <input type="email" name="mail" placeholder="E-mail" class="footer-input">
                <button class="footer-button" type="submit">{{ $blocks['section_callback']['button_text'] }}</button>
            </form>
        </div>
    </div>
    @endif
    <div class="footer-bot">
        <div class="row">
            <div class="col">
                <div>
                    <p class="footer-phone">{{ \Backpack\Settings\app\Models\Setting::get('secondary_phone') }}</p>
                    <p class="footer-phoneText">{{ __('site.phone_subtext') }}</p>
                    <p class="footer-text">
                        E-mail: <a href="mailto:{{\Backpack\Settings\app\Models\Setting::get('email')}}">{{ \Backpack\Settings\app\Models\Setting::get('email') }}</a>
                    </p>
                    <a href="{{ route('sitemap') }}" class="footer-text -mt">Карта сайта</a>

                    <div class="footer-social">
                        <a href="{{ \Backpack\Settings\app\Models\Setting::get('instagram') }}">
                            <img src="/svg/insta.svg" alt="insta icon" style="width: 26px;">
                        </a>
                        <a href="{{ \Backpack\Settings\app\Models\Setting::get('facebook') }}">
                            <img src="/svg/facebook.png" alt="facebook icon" style="width: 31px;">
                        </a>
                    </div>

                    <p class="footer-text -mt">Kazakhstan Travel (c) 2011-2021</p>>
                </div>
            </div>
            <div class="footer-bot-nav">
                <a href="/#tours" class="footer-bot-link">{{ __('site.tours') }}</a>
                <a href="{{ route('reports') }}" class="footer-bot-link">{{ __('site.photos') }}</a>
                <a href="{{ route('reviews') }}" class="footer-bot-link">{{ __('site.reviews') }}</a>
                <a href="{{ route('about') }}" class="footer-bot-link">{{ __('site.about_us') }}</a>
                <a href="{{ route('contacts') }}" class="footer-bot-link">{{ __('site.contacts') }}</a>
            </div>
            <div class="col -jcfe">
                <div>
                    <a href="/" class="footer-logo">
                        <b>Kazakhstan</b> Travel
                    </a>
                    <p class="footer-text">
                        {{ __('site.footer_text') }}
                    </p>
                </div>
            </div>
        </div>
        @if(isset($blocks['section_footer_seo_text']))
            <div class="footer-bot-txt-wrp">
                @foreach($blocks['section_footer_seo_text']['items'] as $item)
                    <a class="footer-link" href="{{ $item['url'] }}">{{ $item['title'] }}</a>
                @endforeach
            </div>
        @endif
    </div>
</footer>
