<section class="mobMenu">
    <div class="mobMenu-top">
        <button class="mobMenu-close"></button>
        <a href="/" class="mobMenu-logo">KZT<span>ravel</span></a>
    </div>
    <div class="mobMenu-langs">
        <a href="">RU</a>
        <a href="">KZ</a>
        <a href="">EN</a>
    </div>
    <nav class="mobMenu-nav">
        <ul class="mobMenu-main">
            <li><a href="/#tours" class="mobMenu-link">{{ __('site.tours') }}</a></li>
            <li><a href="{{ route('reports') }}" class="mobMenu-link">{{ __('site.photos') }}</a></li>
            <li><a href="{{ route('reviews') }}" class="mobMenu-link">{{ __('site.reviews') }}</a></li>
            <li><a href="{{ route('about') }}" class="mobMenu-link">{{ __('site.about_us') }}</a></li>
            <li><a href="{{ route('contacts') }}" class="mobMenu-link">{{ __('site.contacts') }}</a></li>
        </ul>
        <ul class="mobMenu-botList">
            @foreach ($tours as $tour)
                <li><a href="{{ route('tour.show', ['slug' => $tour->slug]) }}">{{ $tour->title }}</a></li>
            @endforeach
        </ul>
    </nav>
</section>
