<section class="reviewsSlider">
    <h4 class="title">{{ __('site.review_title') }}</h4>
    <div class="reviews-slider swiper-container">
        <div class="swiper-wrapper">
            @php
                /** @var \Illuminate\Database\Eloquent\Collection $reviews */
            @endphp

            @foreach($reviews->chunk(4) as $chunk)
                <div class="swiper-slide">
                    @foreach($chunk->chunk(2) as $subChunk)
                        <div class="row">
                            @foreach($subChunk as $item)
                                <div class="col -half">
                                    <div>
                                        <div class="row">
                                            <div class="col {{ $item->video_url !== null ? '-half' : '' }}">
                                                <img src="/{{ $item->bg_image }}" alt="{{ $item->subtitle }}" class="reviewsSlider-image">
                                                <div class="reviewsSlider-inner -comment">
                                                    <img src="/svg/review-icon.svg" alt="Kazakhstan Travel review icon" class="reviewsSlider-icon">
                                                    <div class="row">
                                                        <div class="reviewsSlider-avatar">
                                                            <img src="/{{ $item->user_avatar }}" alt="Kazakhstan Travel avatar">
                                                        </div>
                                                        <div class="reviewsSlider-nameWrp">
                                                            <p class="reviewsSlider-title">{{ $item->user_name }}</p>
                                                            <p class="reviewsSlider-place">{{ $item->subtitle }}</p>
                                                        </div>
                                                    </div>
                                                    <p class="reviewsSlider-text">
                                                        {{ $item->comment }}
                                                    </p>
                                                    <p class="reviewsSlider-date">{{ $item->review_date->toDateString() }}</p>
                                                </div>
                                            </div>
                                            <div class="col -half">
                                                <img src="/{{ $item->video_bg_image }}" alt="{{ $item->subtitle }} 2" class="reviewsSlider-image">
                                                @if ($item->video_url !== null)
                                                    <div class="reviewsSlider-inner -video">
                                                        <a class="reviewsSlider-playBtn" href="{{ $item->video_url }}" target="_blank">
                                                            <img src="/svg/play-icon.svg" alt="Play icon">
                                                        </a>
                                                        <p class="reviewsSlider-title -border">Видео отзыв</p>
                                                        <p class="reviewsSlider-place">{{ $item->subtitle }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div class="reviewsSlider-pagination"></div>
    </div>
</section>
