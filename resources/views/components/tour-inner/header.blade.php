<header class="tour-inner-header">
    @php
        /** @var \App\Models\Tour $tour */
    @endphp
    <div class="tour-inner-header-container">
        <a href="#tour-cost" class="tour-inner-header-link">Что входит в стоимость</a>
        <a href="#tour-details" class="tour-inner-header-link">Детали путешествия</a>
        <a href="#tour-reviews" class="tour-inner-header-link">Отзывы</a>
        <cart-button
            price="{{ $tour->formattedPrice() }}"
            slug="{{ $tour->slug }}"
            :in-cart="{{ (int) $tour->isInCart() }}">
        </cart-button>
    </div>
</header>
