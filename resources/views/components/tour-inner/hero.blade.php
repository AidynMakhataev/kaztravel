<section class="hero -tourInner" style="background-image: url('../{{ $tour->bg_image }}');">
    <div class="overlay"></div>
    <div class="hero-inner">
        <h1 class="hero-title">{{ $tour->title }}</h1>
        <p class="hero-subtitle -mb">{{ $tour->subtitle }}</p>
        <p class="hero-subtitle -mb">{{ $tour->tag_text }}</p>
    </div>
</section>
