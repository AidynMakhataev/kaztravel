<section class="similar">
    <h3 class="title">Другие туры</h3>
    <a href="/#tours" class="similar-subtitle">Посмотреть другие туры</a>
    <div class="swiper-slide tour-inner-similar">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="row">
                    @for($i = 0; $i < 4; $i++)
                        <div class="col">
                            <div class="similar-block">
                                <div class="overlay"></div>
                                <img src="/images/gallery{{$i + 1}}.jpg" alt="Kazakh Travel image" class="similar-img">
                                <div class="similar-blockInner">
                                    <a href="" class="similar-link">
                                        <p class="similar-name">Чарынский каньон</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
            <div class="swiper-slide">
                <div class="row">
                    @for($i = 0; $i < 4; $i++)
                        <div class="col">
                            <div class="similar-block">
                                <div class="overlay"></div>
                                <img src="/images/gallery{{$i + 1}}.jpg" alt="Kazakh Travel image" class="similar-img">
                                <div class="similar-blockInner">
                                    <a href="" class="similar-link">
                                        <p class="similar-name">Чарынский каньон</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>
