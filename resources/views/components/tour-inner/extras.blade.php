@php
    /** @var \App\Models\Tour $tour */
    /** @var array $block */
    $block = $tour->getDecodedUpsellBlock();
@endphp
<div class="tour-extras">
    <div class="tour-extras-inner">
        <h3 class="title">{{ $block['title'] }}</h3>
        @if(false === empty($block['subtitle']))
            <p class="text">{{ $block['subtitle'] }}</p>
        @endif
        <div class="tour-extras-container">
            @foreach(array_chunk($block['items'], 4) as $chunk)
                <div class="row">
                    @foreach($chunk as $item)
                        <upsell-item
                            image="{{ $item['image'] }}"
                            title="{{ $item['title'] }}"
                            price="{{ $item['price'] }}"
                            tour-slug="{{ $tour->slug }}"
                            :in-cart="{{ (int) $tour->isUpsellInCart($item['title']) }}"
                        >
                        </upsell-item>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</div>
