@php
    /** @var \App\Models\Tour $tour */
    /** @var array $block */
    $block = $tour->getDecodedPriceBlock();
@endphp
<section id="tour-cost" class="tourPrice">
    <h3 class="title">{{ $block['title'] }}</h3>
    <div class="tourPrice-price">{{ $block['subtitle'] }}</div>
    <div class="row -jcc">
        @foreach(array_chunk($block['items'], 4) as $chunk)
            <div class="tourPrice-block">
                @foreach($chunk as $item)
                    <p>{{ $item['title'] }}</p>
                @endforeach
            </div>
        @endforeach
    </div>
</section>
