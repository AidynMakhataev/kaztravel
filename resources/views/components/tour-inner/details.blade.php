@php
    /** @var \App\Models\Tour $tour */
    /** @var array $block */
    $block = $tour->getDecodedDetailBlock();
@endphp
<section id="tour-details" class="tourDetails">
    <h3 class="title -tour-inner">{{ $block['title'] }}</span></h3>

    @foreach($block['items'] as $index => $item)
        <div class="row">
            @if($loop->odd)
                <div class="col">
                    <img src="/{{ $item['image'] }}" alt="{{ $item['title'] }}" class="tourDetails-img">
                </div>
                <div class="col -aic">
                    <div class="tourDetails-textWrp">
                        <h6 class="tourDetails-day">{{ $item['title'] }}</h6>
                        <div>
                            {!! $item['description'] !!}
                        </div>
                    </div>
                </div>
            @else
                <div class="col -aic">
                    <div class="tourDetails-textWrp">
                        <h6 class="tourDetails-day">{{ $item['title'] }}</h6>
                        <div>
                            {!! $item['description'] !!}
                        </div>
                    </div>
                </div>
                <div class="col">
                    <img src="/{{ $item['image'] }}" alt="{{ $item['title'] }}" class="tourDetails-img">
                </div>
            @endif
        </div>
    @endforeach
</section>
