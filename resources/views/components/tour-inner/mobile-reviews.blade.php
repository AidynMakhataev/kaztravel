<div class="mobile-reviews">
    <h3 class="title -tour-inner">Отзывы <span> путешественников</span></h3>
    <div class="mobile-reviews-slider swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="/images/reviews-slider1.jpg" alt="reviews image" class="reviewsSlider-image">
                <div class="reviewsSlider-inner -comment">
                    <img src="/svg/review-icon.svg" alt="Kazakhstan Travel review icon" class="reviewsSlider-icon">
                    <div class="row">
                        <div class="reviewsSlider-avatar">
                            <img src="/images/reviews-slider-avatar.jpg" alt="Kazakhstan Travel avatar">
                        </div>
                        <div class="reviewsSlider-nameWrp">
                            <p class="reviewsSlider-title">Анна Анютина</p>
                            <p class="reviewsSlider-place">Тест тест</p>
                        </div>
                    </div>
                    <p class="reviewsSlider-text">
                        Далеко-далеко за словесными, горами в стране гласных и согласных живут рыбные тексты. Языкового дал парадигматическая злых жизни щеке букв, своих напоивший взгляд меня океана текстами грустный, всеми текст заманивший но если? Грамматики.
                    </p>
                    <p class="reviewsSlider-date">2021-11-11</p>
                </div>
            </div>
            <div class="swiper-slide">
                <img src="/images/reviews-slider2.jpg" alt="reviews image 2" class="reviewsSlider-image">
                <div class="reviewsSlider-inner -video">
                    <a class="reviewsSlider-playBtn" href="https://www.youtube.com/watch?v=d0-c0inByMI" target="_blank">
                        <img src="/svg/play-icon.svg" alt="Play icon">
                    </a>
                    <p class="reviewsSlider-title -border">Видео отзыв</p>
                    <p class="reviewsSlider-place">Test test</p>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>
