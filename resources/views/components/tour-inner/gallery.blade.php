<section class="gallery -tourInner">
    <div class="gallery-btnWrp">
        <button class="gallery-add">
            <span>В корзину</span>
        </button>
    </div>
    @php
        /** @var \App\Models\Tour $tour */
        $block = $tour->getDecodedImagesBlock();
    @endphp
    <h3 class="title -tour-inner" style="margin-bottom: 30px;">{{ $block['title'] }}</h3>
    <div class="row" id="tour-inner-gallery">
        @foreach($block['images'] as $image)
        <div class="col">
            <a href="/{{$image['image']}}" class="gallery-block">
                <img src="/{{ $image['image'] }}" alt="{{ $image['title'] }}" class="gallery-blockImg">
                <div class="overlay"></div>
                <div class="gallery-inner">
                    <p class="gallery-title">{{ $image['title'] }}</p>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</section>
