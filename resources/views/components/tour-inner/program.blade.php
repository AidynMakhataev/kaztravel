<section class="tour-map">
    <h3 class="title -tour-inner" style="margin-bottom: 30px;">Карта маршрута</h3>
    <div class="tour-map-map-wrp">
        <img src="/{{ $block['image'] }}" alt="map">
    </div>
</section>
