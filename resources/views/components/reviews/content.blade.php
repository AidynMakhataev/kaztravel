<section class="reviews-content">
    @for ($i = 0; $i < 4; $i++)
        <div class="reviews-row">
            <h3 class="title">Путешествие на Чарынский каньон</h3>
            <div class="reviews-linkWrp">
                <a href="/reports-inner" class="reviews-link">
                    <img src="/svg/camera-icon-white.svg" alt="Kazakh Travel icon">
                    <span>Посмотреть отчет о поездке</span>
                </a>
            </div>
            <a href="/reports-inner" class="reviews-link">
                <img src="/svg/camera-icon.svg" alt="Kazakh Travel icon">
                <span>Посмотреть отчет о поездке</span>
            </a>
            <div class="row">
                <div class="col -jcc -aic">
                    <div class="reviews-colInner">
                        <div class="reviews-avatar">
                            <img src="/images/reviews-slider-avatar.jpg" alt="Kazakh Travel tourist">
                        </div>
                        <p class="reviews-name">Лина Александровна</p>
                        <p class="reviews-text">
                            Прекрасная поездка! Настолько живая и невероятная природа Казахстана! Просто поразительно.
                        </p>
                        <button class="reviews-btn">
                            <img src="/svg/play-icon-green.svg" alt="Kazakh Travel icon">
                            <span>Видеоотзыв</span>
                        </button>
                    </div>
                </div>
                <div class="col -jcc -aic">
                    <div class="reviews-colInner">
                        <div class="reviews-avatar">
                            <img src="/images/reviews-slider-avatar.jpg" alt="Kazakh Travel tourist">
                        </div>
                        <p class="reviews-name">Лина Александровна</p>
                        <p class="reviews-text">
                            Прекрасная поездка! Настолько живая и невероятная природа Казахстана! Просто поразительно.
                        </p>
                        <button class="reviews-btn">
                            <img src="/svg/play-icon-green.svg" alt="Kazakh Travel icon">
                            <span>Видеоотзыв</span>
                        </button>
                    </div>
                </div>
                <div class="col -jcc -aic">
                    <div class="reviews-colInner">
                        <div class="reviews-avatar">
                            <img src="/images/reviews-slider-avatar.jpg" alt="Kazakh Travel tourist">
                        </div>
                        <p class="reviews-name">Лина Александровна</p>
                        <p class="reviews-text">
                            Прекрасная поездка! Настолько живая и невероятная природа Казахстана! Просто поразительно.
                        </p>
                        <button class="reviews-btn">
                            <img src="/svg/play-icon-green.svg" alt="Kazakh Travel icon">
                            <span>Видеоотзыв</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endfor
</section>