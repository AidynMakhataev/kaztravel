<form action="" class="heroForm">
    <h3 class="heroForm-title">Оставить заявку</h3>
    <div class="heroForm-inputWrp">
        <input type="text" name="name" required="required" placeholder="Ваше имя" class="heroForm-input">
        <input type="tel" name="phone" required="required" placeholder="Ваш телефон" class="heroForm-input phone-mask">
        <button type="submit" class="heroForm-button">Купить</button>
    </div>
</form>