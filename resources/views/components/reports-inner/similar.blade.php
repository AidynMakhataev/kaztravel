<section class="similar -reports-inner">
    <h3 class="title">Еще больше <span>путешествий</span></h3>
    <p class="title -mt0"><span>Посмотрите отчеты из наших других экспедиций</span></p>
    <a href="{{ route('reports') }}" class="similar-subtitle -icon">
        <img src="/svg/camera-icon.svg" alt="Kazakh Travel icon">
        <span>Все предыдущие поездки</span>
    </a>
    <div class="swiper-slide reports-inner-similar">
        <div class="swiper-wrapper">
            @foreach($similar as $report)
                @php
                    /** @var \App\Models\Report $report */
                @endphp
            <div class="swiper-slide">
                <div class="similar-block">
                    <div class="overlay"></div>
                    <img src="/{{ $report->bg_image }}" alt="{{ $report->title }}" class="similar-img">
                    <div class="similar-blockInner">
                        <a href="{{ route('reports.show', ['slug' => $report->slug]) }}" class="similar-link -full-height">
                            <p class="similar-name">{{ $report->title }}</p>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
