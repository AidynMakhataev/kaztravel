<div class="reviews-row">
    <h3 class="title">Отзывы <span>о путешествии</span></h3>
    <a href="/reviews" class="reviews-link">
        <img src="/svg/camera-icon.svg" alt="Kazakh Travel icon">
        <span>Посмотреть все отзывы</span>
    </a>
    <div class="row">
        @for($i = 0; $i < 3; $i++)
            <div class="col -jcc -aic">
                <div class="reviews-colInner">
                    <div class="reviews-avatar">
                        <img src="/images/reviews-slider-avatar.jpg" alt="Kazakh Travel tourist">
                    </div>
                    <p class="reviews-name">Лина Александровна</p>
                    <p class="reviews-text">
                        Прекрасная поездка! Настолько живая и невероятная природа Казахстана! Просто поразительно.
                    </p>
                    <button class="reviews-btn">
                        <img src="/svg/play-icon-green.svg" alt="Kazakh Travel icon">
                        <span>Видеоотзыв</span>
                    </button>
                </div>
            </div>
        @endfor
    </div>
</div>
