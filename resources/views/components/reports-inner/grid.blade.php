@php
    /** @var \App\Models\Report $report */
@endphp

<section class="reports">
    <div class="row" id="reports-inner-gallery">
        @foreach($report->images as $image)
            <div class="col">
                <a href="/{{ $image }}" class="reports-block">
                    <img src="/{{ $image }}" alt="{{ $report->title }}" class="reports-img">
                    <div class="overlay"></div>
                    <button class="reports-icon">
                        <img src="/svg/search-icon.svg" alt="Kazakh Travel icon">
                    </button>
                </a>
            </div>
        @endforeach
    </div>
</section>
