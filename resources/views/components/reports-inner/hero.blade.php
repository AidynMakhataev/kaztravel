@php
    /** @var \App\Models\Report $report */
@endphp

<section class="hero -reports-inner -aic" style="background-image: url('../{{ $report->bg_image }}');">
    <div class="overlay"></div>
    <div class="hero-inner">
        <h1 class="hero-title">{{ $report->title }}</h1>
        <h2 class="hero-text">{{ $report->subtitle }}</h2>
    </div>
</section>
