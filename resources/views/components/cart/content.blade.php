<section class="cart">
    <h1 class="title">Список моих путешествий</h1>
    <div class="row">
       <cart-item-list :cart-items="{{ $cartContent }}"></cart-item-list>
        <div class="cart-right">
            @if($cartTotalPrice > 0)
                @include('components.cart.total')
            @endif
        </div>
    </div>
</section>
