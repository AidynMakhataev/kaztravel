<div class="cartRemove">
    <div class="overlay"></div>
    <div class="cartRemove-inner">
        <h4 class="cartRemove-title">Удалить путешествие?</h4>
        <div class="cartRemove-btnWrp">
            <button class="cartRemove-btn">Да</button>
            <button class="cartRemove-btn -inverted">Нет</button>
        </div>
    </div>
</div>