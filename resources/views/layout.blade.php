<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <title>KazTravel</title>
    </head>
    <body class="-opensans">
    <script src="https://www.paypal.com/sdk/js?client-id={{\Backpack\Settings\app\Models\Setting::get('paypal_client_id')}}&currency=USD"></script>

        <div class="wrapper" id="app">
                @include('components.header')
                @include('components.mob-header')
                @include('components.mob-menu')
                <main class="wrapper-content">
                    @yield('content')
                </main>
                @include('components.mob-advs')
                @include('components.footer')
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
        <script>
        if (document.getElementById('paypal-button-container')) {
            paypal.Buttons({

                // Sets up the transaction when a payment button is clicked
                createOrder: function(data, actions) {
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: {{ \Backpack\Settings\app\Models\Setting::get('prepayment_amount') }} // Can reference variables or functions. Example: `value: document.getElementById('...').value`
                            }
                        }]
                    });
                },

                // Finalize the transaction after payer approval
                onApprove: function(data, actions) {
                    return actions.order.capture().then(function(orderData) {

                        axios.post('/orders', {data: orderData})
                            .then(() => window.location.replace('/success'))
                    });
                }
            }).render('#paypal-button-container');
        }
    </script>
    </body>
</html>
