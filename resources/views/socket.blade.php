<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Socket</title>
</head>
<body>
<div class="wrapper">
    <h1>Hello from socket</h1>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/centrifugal/centrifuge-js@2.7.7/dist/centrifuge.min.js"></script>
<script>
    axios.get('http://orders.aidyn.made.kz/orders/v1/socket/connection_data', {
        headers: {
            'X-User': 12005767
        }
    })
        .then((response) => {
            console.log(response.data)

            const clientUrl = response.data.data.client_url
            const token = response.data.data.token

            console.log(clientUrl)
            console.log(token)

            let centrifuge = new Centrifuge(clientUrl, {debug: true})
            centrifuge.setToken(token)

            centrifuge.subscribe("order.paid#12005767", function (message) {
                console.log(message)
            })

            centrifuge.subscribe("order.failed#12005767", function (message) {
                console.log(message)
            })

            centrifuge.on('disconnect', function (context) {
                console.log(context)
            })

            centrifuge.on('connect', function (context) {
                console.log(context)
            })

            centrifuge.connect()
        })
        .catch((error) => {
            console.log(error.response.data)
        })


</script>
</body>
</html>
