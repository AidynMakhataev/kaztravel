<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ route('home') }}</loc>
        <lastmod>{{ \Carbon\CarbonImmutable::create(2021, 10, 31)->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>{{ route('about') }}</loc>
        <lastmod>{{ \Carbon\CarbonImmutable::create(2021, 10, 31)->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>yearly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>{{ route('reviews') }}</loc>
        <lastmod>{{ \Carbon\CarbonImmutable::create(2021, 10, 31)->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>yearly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>{{ route('contacts') }}</loc>
        <lastmod>{{ \Carbon\CarbonImmutable::create(2021, 10, 31)->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>yearly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>{{ route('reports') }}</loc>
        <lastmod>{{ \Carbon\CarbonImmutable::create(2021, 10, 31)->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>yearly</changefreq>
        <priority>0.8</priority>
    </url>
    @foreach ($tours as $tour)
        <url>
            <loc>{{ route('tour.show', ['slug' => $tour->slug]) }}</loc>
            <lastmod>{{ $tour->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
    @foreach ($reports as $report)
        <url>
            <loc>{{ route('reports.show', ['slug' => $report->slug]) }}</loc>
            <lastmod>{{ $report->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>
