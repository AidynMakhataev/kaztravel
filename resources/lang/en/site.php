<?php

return [
    'review_title'  => 'Traveler reviews',
    'tours'         => 'Tours',
    'photos'        => 'Photo reports',
    'reviews'       => 'Reviews',
    'about_us'      => 'About us',
    'contacts'      => 'Contacts',
    'feedback'      => 'Feedback',
    'phone_subtext' => 'Free within Kazakhstan',
    'header_heading_text' => 'Explore the incredible nature of majestic Kazakhstan!',
    'footer_text' => 'Путешествия оплачиваются в тенге и долларах по курсу в день заключения договора',
];
