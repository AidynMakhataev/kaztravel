<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\Admin\CallbackController;
use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'home'])->name('home');
Route::get('/contacts', [MainController::class, 'contacts'])->name('contacts');
Route::get('/reports', [MainController::class, 'reports'])->name('reports');
Route::get('/reports/{slug}', [MainController::class, 'showReport'])->name('reports.show');
Route::get('/tours/{slug}', [TourController::class, 'show'])->name('tour.show');
Route::post('/tours/{slug}/add_to_cart', [TourController::class, 'addToCart'])->name('tour.add_to_cart');
Route::post('/tours/{slug}/add_upsell_item', [TourController::class, 'addUpsellItem'])->name('tour.add_upsell_item');
Route::post('/tours/{slug}/remove_upsell_item', [TourController::class, 'removeUpsellItem'])->name('tour.remove_upsell_item');
Route::get('/about', [MainController::class, 'about'])->name('about');
Route::get('/reviews', [MainController::class, 'reviews'])->name('reviews');
Route::post('/callback', CallbackController::class)->name('callback');

Route::get('/cart', [CartController::class, 'content'])->name('cart');
Route::post('/cart/{rowId}/remove', [CartController::class, 'removeItem'])->name('cart.remove_item');
Route::post('/orders', [\App\Http\Controllers\OrderController::class, 'create'])->name('orders.create');

Route::get('/sitemap.xml', [\App\Http\Controllers\SitemapXmlController::class, 'index'])->name('sitemap');

Route::get('/migrate', function () {
    \Illuminate\Support\Facades\Artisan::call('migrate', [
        '--force' => true,
    ]);

    return 1;
});

//Route::get('/socket', function () {
//    return view('socket');
//});

Route::view('/success', 'pages.success');
