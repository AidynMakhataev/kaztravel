<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tour_id')->nullable()->index();
            $table->string('user_name');
            $table->text('user_avatar');
            $table->string('subtitle');
            $table->text('comment');
            $table->text('bg_image');
            $table->text('video_bg_image');
            $table->string('video_url')->nullable();
            $table->timestamp('review_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
