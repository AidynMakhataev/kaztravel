<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertIntoSettingsFacebookValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('settings')
            ->insert(
                [
                    [
                        'key'           => 'facebook',
                        'name'          => 'Facebook',
                        'description'   => 'Ссылка на Facebook',
                        'value'         => 'https://instagram.com/facebook',
                        'field'         => '{"name":"value","label":"Ссылка","type":"url"}',
                        'active'        => 1,
                    ],
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
