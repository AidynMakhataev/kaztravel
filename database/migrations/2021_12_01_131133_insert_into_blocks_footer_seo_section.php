<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertIntoBlocksFooterSeoSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('blocks')
            ->insert(
                [
                    [
                        'key'        => 'section_footer_seo_text',
                        'name'       => 'Сео ссылки на footer',
                        'fields'     => '[{"fake": true, "name": "items", "type": "repeatable", "label": "Сео текста", "fields": [{"name": "title", "type": "text", "label": "Заголовок", "wrapper": {"class": "form-group col-md-6"}}, {"name": "url", "type": "url", "label": "Ссылка", "wrapper": {"class": "form-group col-md-6"}}], "store_in": "extras"}]',
                        'extras'     => '{"items": "[{\"title\":\"Туры в Африку\",\"url\":\"https://kztravel.com\"},{\"title\":\"Туры в Сомали\",\"url\":\"https://kztravel.com\"},{\"title\":\"Туры в Китай\",\"url\":\"https://kztravel.com\"}]"}',
                        'created_at' => now()->toDateTimeString(),
                    ],
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
