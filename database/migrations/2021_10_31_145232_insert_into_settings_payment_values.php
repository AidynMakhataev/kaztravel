<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertIntoSettingsPaymentValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('settings')
            ->insert(
                [
                    [
                        'key'           => 'prepayment_amount',
                        'name'          => 'Сумма предоплаты заказа',
                        'description'   => 'Сумма которая будет списываться по умолчанию при оформлении заказа (в долларах)',
                        'value'         => '20',
                        'field'         => '{"name":"value","label":"Сумма","type":"number"}',
                        'active'        => 1,
                    ],
                    [
                        'key'           => 'paypal_client_id',
                        'name'          => 'PayPal Client ID',
                        'description'   => 'Client ID для платежной системы PayPal',
                        'value'         => 'AVmVlBoBJIj59cDyUJy_FHvGTk8xNLpYKaJcdN9aFQiOkyNa9RPYYgPMJoStpV4E9c4CSM0A5omz14Wr',
                        'field'         => '{"name":"value","label":"Client ID","type":"text"}',
                        'active'        => 1,
                    ],
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('settings')->whereIn('key', ['prepayment_amount', 'paypal_client_id'])->delete();
    }
}
