<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertIntoBlocksSuccessSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('blocks')
            ->insert(
                [
                    [
                        'key'        => 'section_success',
                        'name'       => 'Страница успешной оплаты',
                        'fields'     => '[{"fake": true, "name": "title", "type": "text", "label": "Заголовок", "store_in": "extras"}, {"fake": true, "name": "subtitle", "type": "text", "label": "Подзаголовок", "store_in": "extras"}]',
                        'extras'     => '{"title": "Путевка успешно приобретена!", "subtitle": "Мы отправим вам билет на почту"}',
                        'created_at' => now()->toDateTimeString(),
                    ],
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('blocks')->where('key', '=', 'section_success')->delete();
    }
}
