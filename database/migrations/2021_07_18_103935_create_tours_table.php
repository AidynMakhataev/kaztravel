<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('subtitle');
            $table->string('slug');
            $table->string('tag_text')->nullable();
            $table->text('bg_image');
            $table->string('price');
            $table->jsonb('images_block')->nullable();
            $table->jsonb('price_block')->nullable();
            $table->jsonb('detail_block')->nullable();
            $table->jsonb('upsell_block')->nullable();
            $table->integer('lft')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('tours');
    }
}
