<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')
            ->insert(
                [
                    [
                        'key'           => 'main_phone',
                        'name'          => 'Основной номер телефона',
                        'description'   => 'Основной номер телефона на сайте (указан в header)',
                        'value'         => '+7 (707) 333 24 41',
                        'field'         => '{"name":"value","label":"Номер","type":"text"}',
                        'active'        => 1,
                    ],
                    [
                        'key'           => 'secondary_phone',
                        'name'          => 'Второй номер телефона',
                        'description'   => 'Второй номер телефона на сайте (указан в footer)',
                        'value'         => '+7 (707) 333 24 41',
                        'field'         => '{"name":"value","label":"Номер","type":"text"}',
                        'active'        => 1,
                    ],
                    [
                        'key'           => 'email',
                        'name'          => 'Email адрес',
                        'description'   => 'Email адрес сайта (заявки будут приходить на эту почту)',
                        'value'         => 'kaztravel@gmail.com',
                        'field'         => '{"name":"value","label":"Email адрес","type":"email"}',
                        'active'        => 1,
                    ],
                    [
                        'key'           => 'instagram',
                        'name'          => 'Instagram',
                        'description'   => 'Ссылка на instagram',
                        'value'         => 'https://instagram.com/kaztravel',
                        'field'         => '{"name":"value","label":"Ссылка на instagram","type":"url"}',
                        'active'        => 1,
                    ],
                    [
                        'key'           => 'instagram',
                        'name'          => 'Instagram',
                        'description'   => 'Ссылка на instagram',
                        'value'         => 'https://instagram.com/kaztravel',
                        'field'         => '{"name":"value","label":"Ссылка на instagram","type":"url"}',
                        'active'        => 1,
                    ],
                ]
            );
    }
}
