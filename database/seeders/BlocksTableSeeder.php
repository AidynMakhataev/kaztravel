<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('blocks')
            ->insert(
                [
                    [
                        'key'        => 'section_hero',
                        'name'       => 'Баннер на главной странице',
                        'fields'     => '[{"fake": true, "name": "title", "type": "text", "label": "Заголовок", "store_in": "extras"}, {"fake": true, "name": "subtitle", "type": "text", "label": "Подзаголовок", "store_in": "extras"}, {"fake": true, "name": "image", "type": "image", "label": "Изображение", "store_in": "extras"}]',
                        'extras'     => '{"image": "/uploads/tours/tour-inner.jpg", "title": "Приключенческие экспедиции", "subtitle": "Самые удивительные места Казахстана"}',
                        'created_at' => now()->toDateTimeString(),
                    ],
                    [
                        'key'        => 'section_advantages',
                        'name'       => 'Наши преимущества',
                        'fields'     => '[{"fake": true, "name": "title", "type": "text", "label": "Заголовок", "store_in": "extras"}, {"fake": true, "name": "subtitle", "type": "text", "label": "Подзаголовок", "store_in": "extras"}, {"fake": true, "name": "items", "type": "repeatable", "label": "Преимущества", "fields": [{"name": "title", "type": "text", "label": "Заголовок", "wrapper": {"class": "form-group col-md-6"}}, {"name": "icon", "type": "browse", "label": "Иконка", "wrapper": {"class": "form-group col-md-6"}}, {"name": "description", "type": "text", "label": "Описание"}], "max_rows": 4, "min_rows": 4, "store_in": "extras"}]',
                        'extras'     => '{"items": "[{\"title\":\"Насыщенная программа\",\"icon\":\"uploads/svg/backpack-icon.svg\",\"description\":\"Максимум впечатлений в каждой поездке — от парада пингвинов в ЮАР до сэндбординга в Сахаре.\"},{\"title\":\"Погружение в страну\",\"icon\":\"uploads/svg/map-icon.svg\",\"description\":\"Мы протестировали каждый маршрут и собрали вишенки с тортов для вас\"},{\"title\":\"Забота о каждом клиенте\",\"icon\":\"uploads/svg/boy-icon.svg\",\"description\":\"Знакомство перед поездкой, авторский путеводитель по стране, консультации и поддержка.\"},{\"title\":\"Договор без уловок\",\"icon\":\"uploads/svg/contract-icon.svg\",\"description\":\"Фиксированная стоимость путешествия. Выполнение программы на 99%.\"}]", "title": "Kazakhstan Travel", "subtitle": "Преимущества путешествий вместе с нами"}',
                        'created_at' => now()->toDateTimeString(),
                    ],
                    [
                        'key'        => 'section_callback',
                        'name'       => 'Связаться с нами',
                        'fields'     => '[{"fake": true, "name": "title", "type": "text", "label": "Заголовок", "store_in": "extras"}, {"fake": true, "name": "subtitle", "type": "text", "label": "Подзаголовок", "store_in": "extras"}, {"fake": true, "name": "button_text", "type": "text", "label": "Текст кнопки", "store_in": "extras"}, {"fake": true, "name": "image", "type": "browse", "label": "Изображение", "store_in": "extras"}]',
                        'extras'     => '{"image": "uploads/images/footer-bg.jpg", "title": "Узнавайте первыми о новых экспедициях", "subtitle": "Оставьте свои контакты, чтобы получать программы новых путешествий", "button_text": "Связаться с нами по почте"}',
                        'created_at' => now()->toDateTimeString(),
                    ],
                    [
                        'key'        => 'section_about_hero',
                        'name'       => 'Баннер на странице О нас',
                        'fields'     => '[{"fake": true, "name": "title", "type": "text", "label": "Заголовок", "store_in": "extras"}, {"fake": true, "name": "subtitle", "type": "text", "label": "Подзаголовок", "store_in": "extras"}, {"fake": true, "name": "image", "type": "browse", "label": "Изображение", "store_in": "extras"}]',
                        'extras'     => '{"image": "uploads/images/about-bg.png", "title": "Kazakhstan Travel", "subtitle": "Увидеть от бескрайних степей, до величественных гор Алматы - вместе с KazakhstanTravel!"}',
                        'created_at' => now()->toDateTimeString(),
                    ],
                    [
                        'key'        => 'section_about_team',
                        'name'       => 'Наша команда специалистов',
                        'fields'     => '[{"fake": true, "name": "title", "type": "text", "label": "Заголовок", "store_in": "extras"}, {"fake": true, "name": "subtitle", "type": "text", "label": "Подзаголовок", "store_in": "extras"}, {"fake": true, "name": "image", "type": "browse", "label": "Изображение", "store_in": "extras"}]',
                        'extras'     => '{"items": "[{\"name\":\"Ксения Собчак\",\"avatar\":\"uploads/images/staff-1.jpeg\",\"position\":\"Бизнес аналитик\"},{\"name\":\"Ксения Собчак\",\"avatar\":\"uploads/images/staff-2.jpeg\",\"position\":\"Бизнес аналитик\"},{\"name\":\"Ксения Собчак\",\"avatar\":\"uploads/images/staff-3.jpeg\",\"position\":\"Бизнес аналитик\"},{\"name\":\"Ксения Собчак\",\"avatar\":\"uploads/images/staff-4.jpeg\",\"position\":\"Бизнес аналитик\"},{\"name\":\"Ксения Собчак\",\"avatar\":\"uploads/images/staff-5.jpeg\",\"position\":\"Бизнес аналитик\"}]", "title": "Наша команда специалистов"}',
                        'created_at' => now()->toDateTimeString(),
                    ],
                ]
            );
    }
}
