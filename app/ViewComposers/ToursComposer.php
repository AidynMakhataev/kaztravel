<?php

declare(strict_types=1);

namespace App\ViewComposers;

use App\Models\Tour;
use Illuminate\View\View;

final class ToursComposer
{
    public function compose(View $view): void
    {
        $tours = Tour::active()->orderBy('lft')->take(15)->get();

        $view->with('tours', $tours);
    }
}
