<?php

declare(strict_types=1);

namespace App\ViewComposers;

use Gloudemans\Shoppingcart\Cart;
use Illuminate\View\View;

final class CartComposer
{
    /** @var Cart */
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function compose(View $view): void
    {
        $view->with('cartContent', $this->cart->content());
        $view->with('cartTotalPrice', $this->cart->total());
        $view->with('cartItemsCount', $this->cart->count());
    }
}
