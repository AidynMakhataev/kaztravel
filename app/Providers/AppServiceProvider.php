<?php

namespace App\Providers;

use App\ViewComposers\CartComposer;
use App\ViewComposers\ToursComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['layout', 'pages.home', 'components.mob_menu'],
            ToursComposer::class,
        );

        View::composer(
            ['*'],
            CartComposer::class,
        );
    }
}
