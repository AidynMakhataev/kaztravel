<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Builder
 */
class Order extends Model
{
    use HasFactory, CrudTrait;

    protected $fillable = [
        'prepaid_price', 'total_price', 'cart_data', 'payment_data'
    ];

    protected $casts = [
        'prepaid_price'     => 'float',
        'total_price'       => 'float',
        'cart_data'         => 'array',
        'payment_data'      => 'array',
    ];
}
