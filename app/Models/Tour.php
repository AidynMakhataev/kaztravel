<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Gloudemans\Shoppingcart\Cart;
use Gloudemans\Shoppingcart\CartItem;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Tour
 * @property int $id
 * @property string $title
 * @property int $price
 * @property string $slug
 * @property string $bg_image
 * @method static Builder active()
 * @method static self whereSlug(string $slug)
 * @mixin Builder
 */
class Tour extends Model implements Buyable
{
    use CrudTrait, Sluggable, SluggableScopeHelpers;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tours';
    protected $guarded = ['id'];

    protected $fakeColumns = [
        'price_block', 'detail_block', 'images_block', 'upsell_block', 'route_map_block'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function isInCart(): bool
    {
        /** @var Cart $cart */
        $cart = app(Cart::class);

        return $cart->search(function ($cartItem) {
            return $cartItem->id === $this->id;
        })->isNotEmpty();
    }

    public function isUpsellInCart(string $upsellTitle): bool
    {
        /** @var Cart $cart */
        $cart = app(Cart::class);

        /** @var CartItem|null $cartItem */
        $cartItem = $cart->search(function ($cartItem) {
            return $cartItem->id === $this->id;
        })->first();

        if ($cartItem === null) {
            return false;
        }

        $chosenUpsellItems = $cartItem->options['chosen_upsell_titles'];

        return in_array(trim($upsellTitle), $chosenUpsellItems, true);
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->title;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function formattedPrice(): string
    {
        return number_format($this->price, 0, ',', ' ');
    }

    public function getDecodedImagesBlock(): ?array
    {
        if ($this->images_block === null) {
            return null;
        }

        $array = json_decode($this->images_block, true);

        if ($array['images_block_title'] === null) {
            return null;
        }

        return [
            'title' => $array['images_block_title'],
            'images' => json_decode($array['images_block_images'], true)
        ];
    }

    public function getDecodedPriceBlock(): ?array
    {
        if ($this->price_block === null) {
            return null;
        }

        $array = json_decode($this->price_block, true);

        if ($array['price_block_title'] === null) {
            return null;
        }

        return [
            'title'     => $array['price_block_title'],
            'subtitle'  => $array['price_block_subtitle'],
            'items'     => json_decode($array['price_block_list'], true)
        ];
    }

    public function getDecodedRouteMapBlock(): ?array
    {
        if ($this->route_map_block === null) {
            return null;
        }

        $array = json_decode($this->route_map_block, true);

        if ($array['route_map_block_title'] === null) {
            return null;
        }

        return [
            'title'     => $array['route_map_block_title'],
            'image'     => $array['route_map_block_image'],
        ];
    }


    public function getDecodedUpsellBlock(): ?array
    {
        if ($this->upsell_block === null) {
            return null;
        }

        $array = json_decode($this->upsell_block, true);

        if ($array['upsell_block_title'] === null) {
            return null;
        }

        return [
            'title'     => $array['upsell_block_title'],
            'subtitle'  => $array['upsell_block_subtitle'],
            'items'     => json_decode($array['upsell_block_items'], true)
        ];
    }

    public function getDecodedDetailBlock(): ?array
    {
        if ($this->detail_block === null) {
            return null;
        }

        $array = json_decode($this->detail_block, true);

        if ($array['detail_block_title'] === null) {
            return null;
        }

        return [
            'title'     => $array['detail_block_title'],
            'items'     => json_decode($array['detail_block_program'], true)
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeActive(Builder $builder): Builder
    {
        return $builder->where('is_active', '=', true);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}
