<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property array $fields
 * @property array $extras
 * @mixin Builder
 */
class Block extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'blocks';
    protected $guarded = ['id'];

    protected $fakeColumns = [
        'extras'
    ];

    protected $casts = [
        'fields' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function get(string $key)
    {
        return self::query()->where('key', '=', $key)->firstOrFail();
    }

    public static function getBlocks(array $keys): array
    {
        $blocks = self::query()->whereIn('key', $keys)->get();

        $items = [];

        foreach ($keys as $key) {
            $block = $blocks->where('key', '=', $key)->first();

            if ($block) {
                $items[$key] = $block->getDecodedAttributes();
            } else {
                $items[$key] = null;
            }
        }

        return $items;
    }

    public function getDecodedAttributes(): array
    {
        $extras = json_decode($this->extras, true);

        if (isset($extras['items'])) {
            $extras['items'] = json_decode($extras['items'], true);
        }

        return $extras;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
