<?php

declare(strict_types=1);

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

trait ImageMutatorTrait
{
    public function saveImage(string $attributeName, string $disk, string $destinationPath, $value)
    {
        // if the image was erased
        if ($value === null) {
            // delete the image from disk
            Storage::disk($disk)->delete($this->{$attributeName});

            // set null in the database column
            $this->attributes[$attributeName] = null;
        }

        // if a base64 was sent, store it in the db
        if (Str::startsWith($value, 'data:image'))
        {
            // 0. Make the image
            $image = Image::make($value)->encode('jpg', 90);

            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';

            // 2. Store the image on disk.
            Storage::disk($disk)->put($destinationPath.'/'.$filename, $image->stream());

            // 3. Delete the previous image, if there was one.
            Storage::disk($disk)->delete($this->{$attributeName});

            // 4. Save the public path to the database
            // but first, remove "public/" from the path, since we're pointing to it
            // from the root folder; that way, what gets saved in the db
            // is the public URL (everything that comes after the domain name)
            $publicDestinationPath = Str::replaceFirst('public/', '', $destinationPath);
            $this->attributes[$attributeName] = $publicDestinationPath.'/'.$filename;
        }
    }

    public function saveFakeImage(string $disk, string $destinationPath, $value)
    {
        if (Str::startsWith($value, 'data:image')) {
            // 0. Make the image
            $image = Image::make($value)->encode('jpg', 90);

            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';

            // 2. Store the image on disk.
            Storage::disk($disk)->put($destinationPath.'/'.$filename, $image->stream());

            $publicDestinationPath = Str::replaceFirst('public/', '/', $destinationPath);

            return $publicDestinationPath . '/' . $filename;
        }

        return $value;
    }
}
