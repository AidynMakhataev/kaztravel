<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Tour;
use Gloudemans\Shoppingcart\Cart;
use Gloudemans\Shoppingcart\CartItem;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TourController extends Controller
{
    public function show(string $slug)
    {
        $tour = Tour::with('reviews')->whereSlug($slug)->first();

        abort_if($tour === null, 404);

        $blocks = Block::getBlocks(['section_advantages', 'section_callback']);

        return view('pages.tour-inner', [
            'tour'      => $tour,
            'blocks'    => $blocks,
        ]);
    }

    public function addToCart(string $slug, Cart $cart): JsonResponse
    {
        /** @var Tour|null $tour */
        $tour = Tour::whereSlug($slug)->active()->first();

        abort_if($tour === null, 404);

        $cartItem = $cart->add(
            [
                'id'      => $tour->id,
                'name'    => $tour->title,
                'qty'     => 1,
                'price'   => $tour->price,
                'options' => [
                    'slug'                  => $tour->slug,
                    'image'                 => $tour->bg_image,
                    'upsell_list'           => $tour->getDecodedUpsellBlock() ? $tour->getDecodedUpsellBlock()['items'] : [],
                    'chosen_upsell_titles'  => [],
                ]
            ]
        );

        return response()->json($cartItem);
    }

    public function addUpsellItem(Request $request, string $slug, Cart $cart): JsonResponse
    {
        $request->validate([
            'title' => 'required',
            'price' => 'required',
        ]);

        /** @var Tour|null $tour */
        $tour = Tour::whereSlug($slug)->active()->first();

        abort_if($tour === null, 404);

        $upsellTitle = $request->input('title');
        $upsellPrice = (int) $request->input('price');

        /** @var CartItem|null $cartItem */
        $cartItem = $cart->search(function (CartItem $item) use ($tour) {
            return $item->id === $tour->id;
        })->first();

        if ($cartItem === null) {
            $price = $tour->price + $upsellPrice;

            $item = $cart->add(
                [
                    'id'      => $tour->id,
                    'name'    => $tour->title,
                    'qty'     => 1,
                    'price'   => $price,
                    'options' => [
                        'slug'                  => $tour->slug,
                        'image'                 => $tour->bg_image,
                        'upsell_list'           => $tour->getDecodedUpsellBlock() ? $tour->getDecodedUpsellBlock()['items'] : [],
                        'chosen_upsell_titles'  => [$upsellTitle],
                    ]
                ]
            );

            return response()->json($item);
        }

        $price = $cartItem->price + $upsellPrice;
        $options = $cartItem->options->toArray();

        $chosenUpsellTitles = $options['chosen_upsell_titles'];
        $chosenUpsellTitles[] = $upsellTitle;

        $options['chosen_upsell_titles'] = array_values(array_unique($chosenUpsellTitles));

        $item = $cart->update(
            $cartItem->rowId,
            [
                'price'     => $price,
                'options'   => $options,
            ]
        );

        return response()->json($item);
    }

    public function removeUpsellItem(Request $request, string $slug, Cart $cart): JsonResponse
    {
        $request->validate([
            'title' => 'required',
            'price' => 'required',
        ]);

        /** @var Tour|null $tour */
        $tour = Tour::whereSlug($slug)->active()->first();

        abort_if($tour === null, 404);

        $upsellTitle = $request->input('title');
        $upsellPrice = (int) $request->input('price');

        /** @var CartItem|null $cartItem */
        $cartItem = $cart->search(function (CartItem $item) use ($tour) {
            return $item->id === $tour->id;
        })->first();

        if ($cartItem === null) {
            return response()->json([]);
        }

        $price = $cartItem->price - $upsellPrice;
        $options = $cartItem->options->toArray();

        $chosenUpsellTitles = array_filter(
            $options['chosen_upsell_titles'],
            static function (string $title) use ($upsellTitle) {
                return $title !== $upsellTitle;
            }
        );

        $options['chosen_upsell_titles'] = array_values($chosenUpsellTitles);

        $item = $cart->update(
            $cartItem->rowId,
            [
                'price'     => $price,
                'options'   => $options,
            ]
        );

        return response()->json($item);
    }
}
