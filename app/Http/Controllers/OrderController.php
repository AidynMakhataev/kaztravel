<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Notifications\OrderCreated;
use Backpack\Settings\app\Models\Setting;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    public function create(Request $request, Cart $cart, Order $orderModel): JsonResponse
    {
        $totalPrice = str_replace(',', '', $cart->total());

        /** @var Order $order */
        $order = $orderModel->create([
            'prepaid_price' => Setting::get('prepayment_amount'),
            'total_price'   => (float) $totalPrice,
            'cart_data'     => $cart->content(),
            'payment_data'  => $request->get('data', []),
        ]);

        Notification::route('mail', Setting::get('email'))
                    ->notifyNow(new OrderCreated($order));

        $cart->destroy();

        return response()->json(['status' => 'success']);
    }
}
