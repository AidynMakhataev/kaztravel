<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Cart;
use Illuminate\Http\JsonResponse;

final class CartController extends Controller
{
    public function content()
    {
        return view('pages.cart');
    }

    public function removeItem(string $rowId, Cart $cart): JsonResponse
    {
        $cart->remove($rowId);

        return response()->json(['success' => true]);
    }
}
