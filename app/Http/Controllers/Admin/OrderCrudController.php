<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

final class OrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->crud->setModel(\App\Models\Order::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/order');
        $this->crud->setEntityNameStrings('заказ', 'заказы');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        $this->crud->addColumn([
            'name'  => 'created_at',
            'label' => 'Дата заказа',
            'type'  => 'datetime'
        ]);

        $this->crud->addColumn([
            'name'      => 'total_price',
            'label'     => 'Общая сумма заказа',
            'type'      => 'closure',
            'function'  => function ($entry) {
                return number_format($entry->total_price, 0, ',', ' ') . '$';
            }
        ]);

        $this->crud->addColumn([
            'name'      => 'prepaid_price',
            'label'     => 'Сумма предоплаты',
            'type'      => 'closure',
            'function'  => function ($entry) {
                return number_format($entry->prepaid_price, 0, ',', ' ') . '$';
            }
        ]);
    }

    protected function setupShowOperation(): void
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->addColumn([
            'name'  => 'total_price',
            'label' => 'Общая сумма заказа',
            'type'      => 'closure',
            'function'  => function ($entry) {
                return number_format($entry->total_price, 0, ',', ' ') . '$';
            }
        ]);

        $this->crud->addColumn([
            'name'  => 'prepaid_price',
            'label' => 'Сумма предоплаты',
            'type'      => 'closure',
            'function'  => function ($entry) {
                return number_format($entry->prepaid_price, 0, ',', ' ') . '$';
            }
        ]);

        $this->crud->addColumn([
            'name'      => 'cart_data',
            'label'     => 'Данные заказа',
            'type'      => 'closure',
            'function'  => function ($entry) {
                $cartData = $entry->cart_data;

                $html = '';

                foreach ($cartData as $data) {
                    $upsellList = implode(', ', $data['options']['chosen_upsell_titles']);

                    $html .= <<<HTML
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <tbody>
            <tr>
                <td>Тур</td>
                <td><a href="/admin/tour/{$data['id']}/edit" target="_blank">{$data['name']}</a></td>
            </tr>
            <tr>
                <td>Цена</td>
                <td>{$data['price']}$</td>
            </tr>
            <tr>
                <td>Выбранные допродажи</td>
                <td>{$upsellList}</td>
            </tr>
        </tbody>
    </table>
HTML;
                }
                return $html;
            }
        ]);

        $this->crud->addColumn([
            'name'      => 'payment_data',
            'label'     => 'Данные платежа с PayPal',
            'type'      => 'closure',
            'function'  => function ($entry) {
                $data = $entry->payment_data;

                return <<<HTML
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <tbody>
            <tr>
                <td>ID заказа</td>
                <td>{$data['id']}</td>
            </tr>
            <tr>
                <td>ФИО покупателя</td>
                <td>{$data['payer']['name']['given_name']} {$data['payer']['name']['surname']}</td>
            </tr>
            <tr>
                <td>Email покупателя</td>
                <td>{$data['payer']['email_address']}</td>
            </tr>
            <tr>
                <td>Статус</td>
                <td>{$data['status']}</td>
            </tr>
            <tr>
                <td>Дата оплаты заказа</td>
                <td>{$data['create_time']}</td>
            </tr>

        </tbody>
    </table>
HTML;
            }
        ]);
    }

}
