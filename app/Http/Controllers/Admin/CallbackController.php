<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Mail\CallbackMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

final class CallbackController
{
    public function __invoke(Request $request): \Illuminate\Http\RedirectResponse
    {
        $data = [
            'name'  => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('mail'),
        ];

        Mail::to('makataev.7@gmail.com')->send(new CallbackMail($data));

        return redirect()->back();
    }
}
