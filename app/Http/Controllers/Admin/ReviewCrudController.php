<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReviewRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class ReviewCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReviewCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->crud->setModel(\App\Models\Review::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/review');
        $this->crud->setEntityNameStrings('отзыв', 'отзывы');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'tour',
            'label'     => 'Тур',
            'type'      => 'relationship',
            'attribute' => 'title',
        ]);

        $this->crud->addColumn([
            'name'      => 'user_name',
            'label'     => 'Имя пользователя',
            'type'      => 'text'
        ]);

        $this->crud->addColumn([
            'name'      => 'user_avatar',
            'label'     => 'Аватар',
            'type'      => 'image'
        ]);

        $this->crud->addColumn([
            'name'      => 'subtitle',
            'label'     => 'Подзаголовок',
            'type'      => 'text'
        ]);

        $this->crud->addColumn([
            'name'      => 'comment',
            'label'     => 'Отзыв',
            'type'      => 'text'
        ]);

        $this->crud->addColumn([
            'name'      => 'review_date',
            'label'     => 'Дата отзыва',
            'type'      => 'date'
        ]);

    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ReviewRequest::class);

        $this->crud->addField([
            'name'      => 'tour',
            'type'      => 'relationship',
            'label'     => 'Тур',
            'attribute' => 'title',
        ]);

        $this->crud->addField([
            'name'  => 'user_name',
            'type'  => 'text',
            'label' => 'Имя пользователя',
        ]);

        $this->crud->addField([
            'name'          => 'user_avatar',
            'type'          => 'browse',
            'label'         => 'Аватар пользователя',
            'mime_types'    => ['image'],
        ]);

        $this->crud->addField([
            'name'          => 'bg_image',
            'type'          => 'browse',
            'label'         => 'Фоновое изображение',
            'mime_types'    => ['image'],
        ]);

        $this->crud->addField([
            'name'          => 'video_bg_image',
            'type'          => 'browse',
            'label'         => 'Фоновое изображение для видео блока',
            'mime_types'    => ['image'],
        ]);

        $this->crud->addField([
            'name'  => 'subtitle',
            'type'  => 'text',
            'label' => 'Подзаголовок',
        ]);

        $this->crud->addField([
            'name'  => 'comment',
            'type'  => 'textarea',
            'label' => 'Текст отзыва',
        ]);

        $this->crud->addField([
            'name'              => 'video_url',
            'type'              => 'url',
            'label'             => 'Ссылка на видео отзыв',
        ]);

        $this->crud->addField([
            'name'  => 'review_date',
            'type'  => 'date',
            'label' => 'Дата отзыва',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
