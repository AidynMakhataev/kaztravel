<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BlockRequest;
use App\Models\Block;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class BlockCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BlockCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->crud->setModel(\App\Models\Block::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/block');
        $this->crud->setEntityNameStrings('блок', 'блоки');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        $this->crud->addColumn([
            'name'  => 'name',
            'type'  => 'text',
            'label' => 'Название'
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation(): void
    {
        /** @var Block $block */
        $block = $this->crud->getCurrentEntry();

        $this->crud->addFields($block->fields);
    }
}
