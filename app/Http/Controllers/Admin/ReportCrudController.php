<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReportRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class ReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->crud->setModel(\App\Models\Report::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report');
        $this->crud->setEntityNameStrings('фотоотчет', 'фотоотчеты');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'title',
            'label'     => 'Название',
            'type'      => 'text'
        ]);

        $this->crud->addColumn([
            'name'      => 'subtitle',
            'label'     => 'Описание',
            'type'      => 'text'
        ]);

        $this->crud->addColumn([
            'name'      => 'is_active',
            'label'     => 'Активен',
            'type'      => 'check'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ReportRequest::class);

        $this->crud->addField([
            'name'      => 'tour',
            'type'      => 'relationship',
            'label'     => 'Тур',
            'attribute' => 'title',
        ]);

        $this->crud->addField([
            'name'  => 'title',
            'type'  => 'text',
            'label' => 'Название',
        ]);

        $this->crud->addField([
            'name'  => 'slug',
            'type'  => 'text',
            'label' => 'URL страницы',
            'hint'  => 'Будет сгенерирован автоматический на основе названия фотоотчета если оставить это поле пустым',
        ]);

        $this->crud->addField([
            'name'  => 'subtitle',
            'type'  => 'text',
            'label' => 'Подзаголовок',
        ]);

        $this->crud->addField([
            'name'       => 'bg_image',
            'type'       => 'browse',
            'label'      => 'Фоновое изображение',
            'mime_types' => ['image'],
        ]);

        $this->crud->addField([
            'name'          => 'images',
            'type'          => 'browse_multiple',
            'label'         => 'Фотографии',
            'mime_types'    => ['image'],
        ]);

        $this->crud->addField([
            'name'  => 'is_active',
            'type'  => 'checkbox',
            'label' => 'Показать фотоотчет на сайте',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'title');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
    }

    public function saveReorder()
    {
        $this->crud->hasAccessOrFail('reorder');

        $all_entries = \Request::input('tree');

        if (count($all_entries)) {
            $count = 0;

            foreach ($all_entries as $key => $entry) {
                if ($entry['item_id'] != '' && $entry['item_id'] != null) {
                    $item = $this->crud->model->find($entry['item_id']);
                    $item->lft = empty($entry['left']) ? null : $entry['left'];
                    $item->save();

                    $count++;
                }
            }
        } else {
            return false;
        }

        return 'success for '.$count.' items';
    }
}
