<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TourRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class TourCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TourCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup(): void
    {
        $this->crud->setModel(\App\Models\Tour::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/tour');
        $this->crud->setEntityNameStrings('тур', 'туры');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        $this->crud->addColumn([
            'name'      => 'title',
            'label'     => 'Название тура',
            'type'      => 'text'
        ]);

        $this->crud->addColumn([
            'name'      => 'subtitle',
            'label'     => 'Описание',
            'type'      => 'text'
        ]);

        $this->crud->addColumn([
            'name'      => 'price',
            'label'     => 'Стоимость',
            'type'      => 'number'
        ]);

        $this->crud->addColumn([
            'name'      => 'is_active',
            'label'     => 'Активен',
            'type'      => 'check'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation(): void
    {
        $this->crud->setValidation(TourRequest::class);

        $this->crud->addField([
            'name'  => 'title',
            'type'  => 'text',
            'label' => 'Название',
            'tab'   => 'Основное'
        ]);

        $this->crud->addField([
            'name'  => 'slug',
            'type'  => 'text',
            'label' => 'URL страницы',
            'hint'  => 'Будет сгенерирован автоматический на основе названия тура если оставить это поле пустым',
            'tab'   => 'Основное'
        ]);

        $this->crud->addField([
            'name'  => 'subtitle',
            'type'  => 'text',
            'label' => 'Подзаголовок',
            'tab'   => 'Основное'
        ]);

        $this->crud->addField([
            'name'  => 'tag_text',
            'type'  => 'text',
            'label' => 'Тег текст',
            'tab'   => 'Основное'
        ]);

        $this->crud->addField([
            'name'       => 'bg_image',
            'type'       => 'browse',
            'label'      => 'Фоновое изображение',
            'tab'        => 'Основное',
            'mime_types' => ['image'],
        ]);

        $this->crud->addField([
            'name'  => 'price',
            'type'  => 'number',
            'label' => 'Стоимость тура',
            'tab'   => 'Основное'
        ]);

        $this->crud->addField([
            'name'  => 'is_active',
            'type'  => 'checkbox',
            'label' => 'Показать тур на сайте',
            'tab'   => 'Основное'
        ]);

        $this->crud->addField([
            'name'      => 'detail_block_title',
            'label'     => 'Заголовок',
            'type'      => 'text',
            'fake'      => true,
            'store_in'  => 'detail_block',
            'tab'       => 'Детальная программа'
        ]);

//        $this->crud->addField([
//            'name'      => 'detail_block_subtitle',
//            'label'     => 'Подзаголовок',
//            'type'      => 'text',
//            'fake'      => true,
//            'store_in'  => 'detail_block',
//            'tab'       => 'Детальная программа'
//        ]);

        $this->crud->addField([
            'name'      => 'detail_block_program',
            'label'     => 'Программа тура',
            'type'      => 'repeatable',
            'fake'      => true,
            'fields'    => [
                [
                    'name'      => 'title',
                    'type'      => 'text',
                    'label'     => 'Заголовок',
                    'wrapper'   => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'      => 'image',
                    'type'      => 'browse',
                    'label'     => 'Изображение',
                    'wrapper'   => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'      => 'description',
                    'type'      => 'tinymce',
                    'label'     => 'Описание',
                ],
            ],
            'store_in'  => 'detail_block',
            'tab'       => 'Детальная программа'
        ]);

        $this->crud->addField([
            'name'      => 'images_block_title',
            'label'     => 'Заголовок',
            'type'      => 'text',
            'fake'      => true,
            'store_in'  => 'images_block',
            'tab'       => 'Фотографии'
        ]);

        $this->crud->addField([
            'name'      => 'images_block_images',
            'label'     => 'Фотографии',
            'type'      => 'repeatable',
            'fake'      => true,
            'fields'    => [
                [
                    'name'      => 'title',
                    'type'      => 'text',
                    'label'     => 'Заголовок',
                    'wrapper'   => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'      => 'image',
                    'type'      => 'browse',
                    'label'     => 'Фото',
                    'wrapper'   => ['class' => 'form-group col-md-6'],
                ],
            ],
            'store_in'  => 'images_block',
            'tab'       => 'Фотографии'
        ]);

        $this->crud->addField([
            'name'      => 'price_block_title',
            'label'     => 'Заголовок',
            'type'      => 'text',
            'fake'      => true,
            'store_in'  => 'price_block',
            'tab'       => 'Что входит в стоимость'
        ]);

        $this->crud->addField([
            'name'      => 'price_block_subtitle',
            'label'     => 'Подзаголовок',
            'type'      => 'text',
            'fake'      => true,
            'store_in'  => 'price_block',
            'tab'       => 'Что входит в стоимость'
        ]);

        $this->crud->addField([
            'name'      => 'price_block_list',
            'label'     => 'Список',
            'type'      => 'repeatable',
            'fake'      => true,
            'fields'    => [
                [
                    'name'      => 'title',
                    'type'      => 'text',
                    'label'     => 'Заголовок',
                    'wrapper'   => ['class' => 'form-group col-md-12'],
                ],
            ],
            'store_in'  => 'price_block',
            'tab'       => 'Что входит в стоимость'
        ]);

        $this->crud->addField([
            'name'      => 'upsell_block_title',
            'label'     => 'Заголовок',
            'type'      => 'text',
            'fake'      => true,
            'store_in'  => 'upsell_block',
            'tab'       => 'Допродажи'
        ]);

        $this->crud->addField([
            'name'      => 'upsell_block_subtitle',
            'label'     => 'Подзаголовок',
            'type'      => 'text',
            'fake'      => true,
            'store_in'  => 'upsell_block',
            'tab'       => 'Допродажи'
        ]);

        $this->crud->addField([
            'name'      => 'upsell_block_items',
            'label'     => 'Предложения',
            'type'      => 'repeatable',
            'fake'      => true,
            'fields'    => [
                [
                    'name'      => 'title',
                    'type'      => 'text',
                    'label'     => 'Заголовок',
                    'wrapper'   => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'      => 'price',
                    'type'      => 'number',
                    'label'     => 'Цена',
                    'wrapper'   => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'      => 'image',
                    'type'      => 'browse',
                    'label'     => 'Фото',
                    'wrapper'   => ['class' => 'form-group col-md-4'],
                ],
            ],
            'store_in'  => 'upsell_block',
            'tab'       => 'Допродажи'
        ]);

        $this->crud->addField([
            'name'      => 'route_map_block_title',
            'label'     => 'Заголовок',
            'type'      => 'text',
            'fake'      => true,
            'store_in'  => 'route_map_block',
            'tab'       => 'Карта маршрута'
        ]);

        $this->crud->addField([
            'name'      => 'route_map_block_image',
            'label'     => 'Фото',
            'type'      => 'browse',
            'fake'      => true,
            'store_in'  => 'route_map_block',
            'tab'       => 'Карта маршрута'
        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation(): void
    {
        $this->setupCreateOperation();
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'title');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
    }

    public function saveReorder()
    {
        $this->crud->hasAccessOrFail('reorder');

        $all_entries = \Request::input('tree');

        if (count($all_entries)) {
            $count = 0;

            foreach ($all_entries as $key => $entry) {
                if ($entry['item_id'] != '' && $entry['item_id'] != null) {
                    $item = $this->crud->model->find($entry['item_id']);
                    $item->lft = empty($entry['left']) ? null : $entry['left'];
                    $item->save();

                    $count++;
                }
            }
        } else {
            return false;
        }

        return 'success for '.$count.' items';
    }
}
