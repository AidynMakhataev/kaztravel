<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Tour;
use Illuminate\Http\Response;

class SitemapXmlController extends Controller
{
    public function index(Tour $tourModel, Report $reportModel): Response
    {
        $tours = $tourModel->active()->get();

        $reports = $reportModel->active()->get();

        return response()->view('sitemap', [
            'tours'     => $tours,
            'reports'   => $reports,
        ])->header('Content-Type', 'text/xml');
    }
}
