<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Report;
use App\Models\Review;

class MainController extends Controller
{
    public function home()
    {
        $blocks = Block::getBlocks(['section_advantages', 'section_hero', 'section_callback', 'section_footer_seo_text']);

        $reviews = Review::take(8)->get();

        return view('pages.home', [
            'blocks'        => $blocks,
            'reviews'       => $reviews,
        ]);
    }

    public function contacts()
    {
        $blocks = Block::getBlocks(['section_callback', 'section_footer_seo_text']);

        return view('pages.contacts', [
            'blocks' => $blocks,
        ]);
    }

    public function reports()
    {
        $reports = Report::active()->orderBy('lft')->get();

        $blocks = Block::getBlocks(['section_callback', 'section_advantages', 'section_footer_seo_text']);

        return view('pages.reports', [
            'blocks'    => $blocks,
            'reports'   => $reports,
        ]);
    }

    public function showReport(string $slug)
    {
        $report = Report::whereSlug($slug)->active()->with('tour')->first();

        abort_if($report === null, 404);

        $blocks = Block::getBlocks(['section_advantages', 'section_callback']);

        $similar = Report::whereSlugNotEqual($slug)->active()->take(4)->get();

        return view('pages.reports-inner', [
            'report'    => $report,
            'blocks'    => $blocks,
            'similar'   => $similar,
        ]);
    }

    public function about()
    {
        $blocks = Block::getBlocks(['section_advantages', 'section_callback', 'section_about_hero', 'section_about_team', 'section_footer_seo_text']);

        return view('pages.about', [
            'blocks' => $blocks,
        ]);
    }

    public function reviews()
    {
        $blocks = Block::getBlocks(['section_advantages', 'section_callback', 'section_footer_seo_text']);

        $reviews = Review::take(8)->get();

        return view('pages.reviews', [
            'blocks'    => $blocks,
            'reviews'   => $reviews,
        ]);
    }
}
